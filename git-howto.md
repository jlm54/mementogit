# git - HowTo

Git permet de suivre les évolutions du contenu d'un repertoire/dossier. Git est un système de gestion de fichiers.

## Création d'un suivi
On peut écrire un premier texte : ce fichier en train d'être éditer. On enregistre le fichier dans un dossier. Maintenant on veut suivre l'évolution de ce fichier et sans doute d'autres fichiers qui lui seront associés. Pour ce faire on initialise le suivi dans notre dossier. On ouvre un terminal pour saisir la première commande git : **git init**.

A la suite de la commande un dossier "caché" *.git* est créer dans notre dossier de projet. Ce dossier contient tout l'historique de voter projet. Surtout ne pas le supprimer ou essayer d'y modifier quelque chose.

## Quel est l'état du projet ?
Pour voir l'état d'un projet, on se place à la racine du projet (la où se situe un dossier caché .git) et on tape la commande **git status**.

Intéressant à noter : par défaut ou est sur un branche nommé *master*.

## Comment ajouter un fichier dans le suivi ?
La commande pour ajouter un fichier dans le suivi est : **git add nomDuFichier**. En fait nom du fichier est plutôt à comprendre comme chemin (au sens unix) à suivre en relatif à partir du dossier de suivi. Souvent au départ, on utilisera la commande **git add .** où le point correspond au dossier courant est espérant qu'on est bien dans le dossier racine du projet.

Après la commande rien de remarquable ne s'est opéré, mais la commande **git status** nous informe que le fichier git-howto.md (dans son dernier état enregistré, i.e. sans ce texte qui je suis en train d'écrire) sera validé prochainement.

## Comment valider un état du projet
La commande est **git commit -m "message à l'intention de la communauté"**. Les commits ne sont considéré que dans le dossier du développeur. On peut tout transformer si besoin. Un fois les commit publiés (on verra ça plus tard) il ne faut surtout plus y toucher.

Avant de valider le premier commit, il faut se faire connaître : ajout son nom et son adresse email avec les commandes suivantes : 

    git config user.email "Vous@exemple.com"
    git config user.name "Votre Nom"

Ces informations sont enregistrés dans le fichier .git/config

## Comment voir l'histoire d'un projet ?
Git enregistre un histoire à travers la succession des commit et permet de l'afficher avec la commande **git log**

## Comment partager mon projet ?
On va créer un dépôt distant. Dans un premier on le fait sur notre machine, mais si on a un serveur à disposition on suit le même raisonnement.

On crée un dossier pour héberger notre dépôts distant (*mementoGit*). Et dans se dossier on initialise un dépôt git de type *--bare* avec la commande :

    git init --bare

On obtient un dossier contenant l'ensemble des dossiers que l'on retrouve dans le dossier .git de notre projet. Mais ici ce n'est pas un dossier caché.

On veux publier notre projet dans ce dossier/dépôt distant. On va donc enregistrer dans notre fichier *.git/config* l'adresse du dépôt distant et la branche qui y sera publiée dans un bloc *remote* :

    [remote "mementoGit"]
        url = file:///tmp/guest-lfs2cp/Public/MesDepotsGit/mementoGit/
        fetch = +refs/heads/*:refs/remotes/mementoGit/*

Cette modification peut être faite avec une commande git *git remote add -t brach name url*

    git remote add -t dev local file:///tmp/guest-wq9f2p/Public/MesDepotsGit/sauveMemento

Puis on peut publier notre travail avec la commande **git push**. Cependant pour le premier push il faut préciser que l'on va le faire vers le dépôt distant qui l'on vient de configurer et préciser la branche que sera publiée : 

    git push --set-upstream mementoGit master

Les publications suivantes seront avec la commande simplifié : **git push**

## création d'un branche de travail

    git branch dev
    git checkout dev

## merge d'une branche de travail sur la branch master

    git commit -m "" sur la branche courante (dev)
    git push (éventuel)
    git checkout master
    git merge dev
    
### Publication sur un serveur externe
Après avoir créer un projet vierge dans gitlab, il est possible d'ajouter ce nouveau dépôt dans notre projet avec la commande :

    git remote add origin https://gitlab.com/jlm54/mementogit.git

Encore faut-il dire que le push doit se faire sur ce dernier. On corige donc dans *.git/config*

## Comment voir les différence ?
Après avoir enregistré mon texte précédent, on constate avec **git status** (ou avec le petit M à coté de notre nom de fichier dans codium), que le même fichier est dans les section à valider et non pris encore en compte. Pour voir la différence entre les 2 version on utilise la commande **git diff**

Git maintient un état dit indexé (un index) des modifications qui seront prises en compte au moment de la validation : le commit.


## Comment pour annuler une modification indexée.
Il se peut que l'on ai mis trop tôt un fichier dans l'index (*git add*). Pour le supprimer de l'index la commande est : **git rm --cached cheminASortirDeLindex**.

