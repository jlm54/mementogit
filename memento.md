# liste des commande git (dans l'ordre d'utilisation)
* git init  # Permet de créer un nouveau dossier de suivi
* **git status**
* **git add cheminLinux**
* **git commit -m "message à l'intention de la communauté"**
* **git log**
* git init --bare # Crée un dossier git distant. C'est l'opération exécutée lorque l'on crée un dossier dans gitlab ou dans github.
man git
* **git --help** #donne toutes les commandes reconnues par git
* **git *commande* --help**
* git remote add -t dev local file:///tmp/guest-wq9f2p/Public/DepotsGit/sauveMemento
* **git push**
* git push --set-upstream mementoGit master

* **git branch dev**
* **git checkout dev**
* **git checkout master**
* **git merge dev**

* **git diff**
* git rm --cached cheminASortirDeLindex
* **git commit --amend**

* **git pull**

* git stash
* git stash apply

